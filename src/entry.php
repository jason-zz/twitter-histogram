<?php
include_once 'load.php';

$app = new Silex\Application();

/*
 * 1st Endpoint /
 */
$app->get('/', function ($name) use ($app)
{
	return 'Try /hello/:name';
});

/*
 * 2nd Endpoint /hello/BarackObama
 */
$app->get('/hello/{twitterName}', function ($twitterName) use ($app)
{
	$twitterService = new JZ\TwitterService();

	$ok = $twitterService->checkTwitterUser($twitterName);

	if($ok){
		return 'Hello '.$app->escape($twitterName);
	}else{
		return 'N/A';
	}
});

/*
 * 3rd Endpoint /histogram/Ferrari
 */
$app->get('/histogram/{twitterName}', function ($twitterName) use ($app)
{
	$twitterHistogram = new JZ\TwitterHistogram();

	$data = $twitterHistogram->getHistogramData($twitterName);

	if($data){
		return $data;
	}else{
		return 'N/A';
	}
});

$app->run();