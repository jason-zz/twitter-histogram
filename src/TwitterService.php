<?php

namespace JZ;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * Basic Twitter API service.
 * 
 * @author Jason Zhang
 */
class TwitterService 
{
	/**
	 * @var string
	 */
	private $consumerKey;
	
	/**
	 * @var string
	 */
	private $consumerSecret;
	
	/**
	 * @var string
	 */
	private $accessToken;
	
	/**
	 * @var string
	 */
	private $accessTokenSecret;
	
	public function __construct()
	{
		try {
			$yaml = Yaml::parse(file_get_contents(__DIR__ . '/_config/twitter.yml'));
			
			$this->setupAuth($yaml);
		} catch (ParseException $e) {
			printf("Unable to parse the YAML string: %s", $e->getMessage());
		}
	}
	
	/**
	 * @param array $yaml
	 */
	private function setupAuth($yaml)
	{
		if(count($yaml))
		{
			$this->accessToken = $yaml['AccessToken'];
			$this->accessTokenSecret = $yaml['AccessTokenSecret'];;
			$this->consumerKey = $yaml['ConsumerKey'];
			$this->consumerSecret = $yaml['ConsumerSecret'];
		}
	}
	
	/**
	 * @var TwitterOAuth
	 */
	protected $connection = null;
	
	/**
	 * @return TwitterService
	 */
	private function setupConnection()
	{
		$this->connection = new TwitterOAuth(
			$this->consumerKey, 
			$this->consumerSecret,
			$this->accessToken, 
			$this->accessTokenSecret
		);
		
		return $this;
	}
	
	/**
	 * @return TwitterOAuth
	 */
	public function getConnection()
	{
		if($this->connection === null){
			$this->setupConnection();
		}
		
		return $this->connection;
	}
	
	/**
	 * Loop up Twitter user by screen name.
	 * 
	 * @link https://dev.twitter.com/rest/reference/get/users/lookup
	 * 
	 * @param string $userScreenName
	 * @return boolean
	 */
	public function checkTwitterUser($userScreenName)
	{
		if (empty($userScreenName)) 
			return false;
		
		// Call rest api
		$arguments = array(
			'screen_name' => $userScreenName
		);
		
		$response = $this->getConnection()->get("users/lookup", $arguments);

		// check response 
		if( ! $response || isset($response->errors) || ! is_array($response)){
			return false;
		}

		if ($response[0]->id && $response[0]->screen_name == $userScreenName) {
			return true;
		}
	}
	
	/**
	 * Get tweets by user name.
	 *
	 * @link https://dev.twitter.com/rest/reference/get/statuses/user_timeline
	 *
	 * @param string $userScreenName
	 * @return array | boolean false
	 */
	public function getUserTweets($userScreenName)
	{
		//check user
		if( ! $this->checkTwitterUser($userScreenName)) 
			return false;
	
		// Call rest api
		$arguments = array(
			'screen_name' 		=> $userScreenName,
			'exclude_replies' 	=> false,
			'count'				=> 200
		);
	
		$response = $this->getConnection()->get("statuses/user_timeline", $arguments);

		// check response
		if( ! $response || isset($response->errors) || ! is_array($response)){
			return false;
		}
	
		return $response;
	}
	
	/**
	 * Convert array to JSON string.
	 * 
	 * @param array $value
	 * @return string
	 */
	protected function arrayToJson($value)
	{
		return json_encode($value);
	}
}
