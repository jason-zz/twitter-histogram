<?php
namespace JZ;

use JZ\TwitterService;

/**
 * 
 * @author Jason Zhang
 */
class TwitterHistogram extends TwitterService 
{
	/**
	 * Loop up Twitter user by screen name.
	 * 
	 * @link https://dev.twitter.com/rest/reference/get/users/lookup
	 * 
	 * @param string $userScreenName
	 * @return boolean
	 */
	public function getHistogramData($userScreenName)
	{
		//get tweets
		$tweets = $this->getUserTweets($userScreenName);
		
		if( ! $tweets) 
			return false;
		
		$arrayData = $this->buildHistogramArray($tweets);
		
// 		var_dump($arrayData);die;
		
		return $this->arrayToJson($arrayData);
	}
	
	/**
	 * @param array $tweets
	 * @return array
	 */
	protected function buildHistogramArray($tweets)
	{
		$data = array();
		
		if(is_array($tweets) && ! empty($tweets)){
			foreach($tweets as $tweet){
				$dateDetails = date_parse($tweet->created_at);
				
				//TODO timezone.
				
				$d = sprintf('%d-%d-%d', $dateDetails['year'], $dateDetails['month'], $dateDetails['day']);
				if( ! isset($data[$d])){
					$data[$d] = array();
				}
				
				$h = $dateDetails['hour'];
				if( ! isset($data[$d][$h])){
					$data[$d][$h] = 1;
				}else{
					$data[$d][$h]++;
				}
			}
		}
		
		return $data;
	}
	
	
}
