<?php
namespace JZ;

use JZ\TwitterService;
use JZ\TwitterHistogram;

/**
 * @author Jason Zhang
 */
class TwitterHistogramTest extends \PHPUnit_Framework_TestCase
{
	public function testConnection()
	{
		$twitter = new TwitterService();
	
		$returnValue = $twitter->getConnection();
		$this->assertInstanceOf('Abraham\TwitterOAuth\TwitterOAuth', $returnValue);
	}
	
	public function testValidTwitterUser()
	{
		$twitter = new TwitterService();
	
		$returnValue = $twitter->checkTwitterUser('BarackObama');
		$this->assertTrue($returnValue);
	}
	
	public function testInvalidTwitterUser()
	{
		$twitter = new TwitterService();
	
		$returnValue = $twitter->checkTwitterUser('BarackObamajdkfjkdjfk322340238402242');
		$this->assertFalse($returnValue);
	}
	
	public function testFailedTwitterHistogramJsonData()
	{
		$twitter = new TwitterService();
	
		$returnValue = $twitter->getUserTweets('Ferraridfdfdf32323');
		$this->assertFalse($returnValue);
	}
	
	public function testSuccessTwitterHistogramJsonData()
	{
		$twitter = new TwitterService();
	
		$returnValue = $twitter->getUserTweets('Ferrari');
		$this->assertArrayHasKey('0', $returnValue);
	}
	
}
