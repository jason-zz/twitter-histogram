## Twitter Histogram

## Requirements ##

 * [Ubuntu 14.04 Server](https://www.ubuntu.com/download/alternative-downloads)
 * Apache 2.4+
 * PHP 5.6
 * php-curl extension
 * php-xml extension
 * php-mbstring extension
 * [Composer](http://getcomposer.org)

## Installation ##

 * Install [Composer](http://getcomposer.org) globally. 
 * Go to Apache virtual host document root directory /var/www/html.
 * Checkout project file 
```bash
$ git clone git@bitbucket.org:jason-zz/twitter-histogram.git .
```
 * Install PHP dependencies
```bash
$ composer install
```

## Usage ##
There are 3 endpoints for this application.

1. http://localhost/
⋅⋅⋅Return instruction how to use 2nd endpoint.

2. http://localhost/hello/BarackObama 
⋅⋅⋅Validate twitter user with screen name **BarackObama**. It returns `Hello BarackObama` if user exists.

3. http://localhost/histogram/Ferrari
⋅⋅⋅Return JSON string displaying the number of tweets per hour of each date in the most recent 200 tweets.

**BarackObama** and **Ferrari** are twitter screen names. You can replace them with any valid twitter user's screen name.

## Tests ##

```bash
$ vendor/phpunit/phpunit/phpunit --bootstrap src/load.php test
```

## Demo ##
1. Web Application [http://ec2-52-64-23-148.ap-southeast-2.compute.amazonaws.com:32772/twitter-histogram/](http://ec2-52-64-23-148.ap-southeast-2.compute.amazonaws.com:32772/twitter-histogram/)

2. Cloud IDE
* [http://ec2-52-64-23-148.ap-southeast-2.compute.amazonaws.com/dashboard/#/ide/che/php](http://ec2-52-64-23-148.ap-southeast-2.compute.amazonaws.com/dashboard/#/ide/che/php)
* Username - user
* Password - byPqS3F8Qqab
